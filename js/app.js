angular.module('vapor-tools', ['ngRoute', 'dataService', 'nav'])
    .config(['$provide', 'Start', function($provide, Start) {
        var appConfig = Start();

        for(item in appConfig) {
            $provide.constant(item, appConfig[item]);
        }
    }])
    .run(function(appInfo) {
        console.log('Anwendung gestartet!');
        for(var key in appInfo) {
            console.log('  ' + key + ': ' + appInfo[key]);
        }
    });